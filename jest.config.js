/* eslint-disable */
module.exports = {
    "roots": [
        "<rootDir>"
    ],
    "transform": {
        "^.+\\.tsx?$": "ts-jest"
    },
    coveragePathIgnorePatterns:[
        "/repositories",
        "/migrations",
        "/routes",
        "/models",
        "/custom.d.ts",
        "server.ts"
    ],
    "globals": {
        "ts-jest": {
            "diagnostics": false
        }
    }
};