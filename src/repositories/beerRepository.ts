import {Beer} from "../models/beer";
import {getRepository} from "typeorm";
import moment = require("moment");

let beerRepository = {
    all: () => {
        return getRepository(Beer).find();
    },

    createOne: async(name: string, beerTypeId: number, brandId: number) => {
        let insertResult = await getRepository(Beer).insert(
            {
                name,
                type: {id: beerTypeId},
                brand: {id: brandId},
                createdAt: moment(),
                updatedAt: moment()
            }
        );
        return beerRepository.findOne(insertResult.identifiers[0].id);
    },

    findOne: (beerId: number) => {
        return getRepository(Beer).findOne(beerId);
    },
};

export default beerRepository;
