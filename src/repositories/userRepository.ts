import {getRepository} from "typeorm";
import {User, UserRole} from "../models/user";
import moment from "moment";
import {BeerEvaluation} from "../models/beerEvaluation";

let findOneQuery = async (withPassword: boolean) => {
    let userQuery = await getRepository(User).createQueryBuilder("users");
    if (withPassword) {
        userQuery.addSelect("users.password");
    }
    return userQuery;
};

let userRepository = {

    all: () => {
        return getRepository(User).find();
    },

    findOneByEmail: async(email: string, withPassword: boolean = false) => {
        let userQuery = await findOneQuery(withPassword);
        userQuery.where("users.email = :email", {email});
        return userQuery.getOne();
    },

    findOneById: async(id: number, withPassword: boolean = false) => {
        let userQuery = await findOneQuery(withPassword);
        userQuery.where("users.id = :id", {id});
        return userQuery.getOne();
    },

    findOneByUsername: async(username: string, withPassword: boolean = false) => {
        let userQuery = await findOneQuery(withPassword);
        userQuery.where("users.username = :username", {username});
        return userQuery.getOne();
    },

    createOne: async(username: string, password: string, email: string, role: UserRole) => {
        let userRepo = getRepository(User);
        let insertResult = await userRepo.insert(
            {username, password, email, role, createdAt: moment(), updatedAt: moment()}
        );
        return userRepository.findOneById(insertResult.identifiers[0].id);
    },

    getEvaluations: (userId: number) => {
        let beerEvalRepo = getRepository(BeerEvaluation);
        return beerEvalRepo.find({where: {userId}});
    }
};
export default userRepository;
