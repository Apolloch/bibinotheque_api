import app from "./app";
import Express = e.Express;
import e = require("express");
import {createConnection} from "typeorm";

createConnection().then(
    () => {
        app.listen(process.env.PORT, () => {
            //noinspection TsLint
            // tslint:disable-next-line
            console.log("App listening on port 3000");
        });
    }
);
