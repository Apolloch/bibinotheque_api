import e from "express";
import NextFunction = e.NextFunction;
import Request = e.Request;
import Response = e.Response;
import userService from "../services/userService";
import {check} from "express-validator";
import {addValidationErrorHandler} from "../utils/validation";
import {User} from "../models/user";
import UnauthorizedException from "../exceptions/UnauthorizedException";
import {wrap} from "../utils/controller";

export default {

    all: wrap(async(req: Request, res: Response, next: NextFunction) => {
        userService.all()
            .then((users) => res.json(users));
    }),

    auth: wrap(async(req: Request, res: Response, next: NextFunction) => {
        userService.authenticate(req.body.email, req.body.password)
            .then(({token, user}: {token: string, user: User}) => {
                if (user) {
                    res.json({token, user});
                } else {
                    next(new UnauthorizedException());
                }
            });
    }),

    register: wrap(async(req: Request, res: Response, next: NextFunction) => {
        return userService.register(req.body.username, req.body.password, req.body.email).then(
            (user) => res.json(user)
        );
    }),

    getEvaluation: wrap(async(req: Request, res: Response, next: NextFunction) => {
        return userService.getEvaluations(req.params.userId).then(
            (evaluations) => res.json(evaluations)
        );
    }),

    findOne: wrap(async(req: Request, res: Response, next: NextFunction) => {
        return userService.findOne(req.params.userId).then(
            (user) => res.json(user)
        );
    }),

    validateAuth: () => {
        return addValidationErrorHandler([
            check("email").exists().isEmail(),
            check("password").exists().not().isEmpty()
        ]);
    },

    validateRegister: () => {
        return addValidationErrorHandler([
            check("email").exists().isEmail(),
            check("password").exists().not().isEmpty(),
            check("username").exists().not().isEmpty()
        ]);
    },

};
