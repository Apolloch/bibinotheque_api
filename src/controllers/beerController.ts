import NextFunction = e.NextFunction;
import Request = e.Request;
import Response = e.Response;
import {wrap} from "../utils/controller";
import e = require("express");
import beerService from "../services/beerService";
import {addValidationErrorHandler} from "../utils/validation";
import {check} from "express-validator/src";

export default {

    all: wrap(async(req: Request, res: Response, next: NextFunction) => {
        beerService.all()
            .then((beers) => res.json(beers));
    }),

    createOne: wrap(async(req: Request, res: Response, next: NextFunction) => {
        beerService.createOne(req.body.name, req.body.beerTypeId, req.body.brandId)
            .then((beer) => res.json(beer));
    }),

    validateCreateOne: () => {
        return addValidationErrorHandler([
           check("name").exists().not().isEmpty(),
           check("beerTypeId").exists().isNumeric({no_symbols: true}),
           check("brandId").exists().isNumeric({no_symbols: true})
        ]);
    }

};
