import NextFunction = e.NextFunction;
import Response = e.Response;
import Request = e.Request;
import {validationResult} from "express-validator/src";
import e = require("express");

export function handleValidationErrors(req: Request, res: Response, next: NextFunction) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array({ onlyFirstError: true }) });
    }
    return next();
}
