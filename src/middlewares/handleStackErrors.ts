import NextFunction = e.NextFunction;
import Request = e.Request;
import Response = e.Response;
import e = require("express");
import BaseException from "../exceptions/BaseException";

//noinspection JSUnusedLocalSymbols
export function handleStackErrors(err: Error, req: Request, res: Response, next: NextFunction) {
    let payload;
    let status = 500;
    if (isBaseException(err)) {
        let baseException = <BaseException> err;
        payload = {
            message: baseException.message,
            code: baseException.code
        };
        status = baseException.status;
    } else {
        payload = {
            message : process.env.ENV !== "prod" ? err.message : "Something broke!"
        };
    }
    res.status(status).json(payload);
}

function isBaseException(err: Error) {
    return err instanceof BaseException;
}
