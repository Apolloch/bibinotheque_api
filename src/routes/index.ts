import express from "express";
import usersRoutes from "./users";
import beersRoutes from "./beers";

const routes = express.Router();

routes.get("/", (req, res) => {
    res.status(200).json({ message: "Connected!"});
});
routes.use("/users", usersRoutes);
routes.use("/beers", beersRoutes);

export default routes;
