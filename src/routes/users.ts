import express from "express";
import userController from "../controllers/userController";
const routes = express.Router();

routes.get("/", userController.all);
routes.get("/:userId", userController.findOne);
routes.get("/:userId/evaluations", userController.getEvaluation);
routes.post("/auth", userController.validateAuth(), userController.auth);
routes.post("/", userController.validateRegister(), userController.register);

export default routes;
