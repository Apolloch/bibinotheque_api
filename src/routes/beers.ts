import express from "express";
import beerController from "../controllers/beerController";
const routes = express.Router();

routes.get("/", beerController.all);
routes.post("/", beerController.validateCreateOne(), beerController.createOne);

export default routes;
