import userRepository from "../repositories/userRepository";
import {User, UserRole} from "../models/user";
import {UsernameAlreadyExistsException} from "../exceptions/UsernameAlreadyExistsException";
import {EmailAlreadyExistsException} from "../exceptions/EmailAlreadyExistsException";
import {createJwt} from "../utils/jwt";
import {checkPassword, hashPassword} from "../utils/crypt";

export default {

    all: () => {
        return userRepository.all();
    },

    findOne: (userId: number) => {
        return userRepository.findOneById(userId);
    },

    getEvaluations: (userId: number) => {
        return userRepository.getEvaluations(userId);
    },

    authenticate: async(email: string, password: string) => {
        let user: User = await userRepository.findOneByEmail(email, true);
        if (!user) {
            return false;
        }
        if (!await checkPassword(password, user.password)) {
            return false;
        }
        delete user.password;
        let token = createJwt(user);
        return {token, user};
    },

    register: async(username: string, password: string, email: string, role: UserRole = UserRole.BASIC) => {
        if (await userRepository.findOneByEmail(email)) {
            throw new EmailAlreadyExistsException();
        } else if (await userRepository.findOneByUsername(username)) {
            throw new UsernameAlreadyExistsException();
        }
        password = await hashPassword(password);
        return userRepository.createOne(username, password, email, role);
    }
};
