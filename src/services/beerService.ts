import beerRepository from "../repositories/beerRepository";

export default {
    all: () => {
        return beerRepository.all();
    },
    createOne: (name: string, beerTypeId: number, brandId: number) => {
        return beerRepository.createOne(name, beerTypeId, brandId);
    }
};
