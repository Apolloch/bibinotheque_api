import bcrypt from "bcryptjs";
export function hashPassword(password: string) {
    return bcrypt.hash(password, 10);
}

export function checkPassword(unencryptedPassword: string, cryptedPassword: string) {
    return bcrypt.compare(unencryptedPassword, cryptedPassword);
}
