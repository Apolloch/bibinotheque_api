/**
 * Wrap a route handler into a function that catch any async errors and follows it to the next function,
 * so there is no need to catch each service
 * @param fn the route handler
 */
export const wrap = (fn: Function) => (...args: any[]) => fn(...args).catch(args[2]);
