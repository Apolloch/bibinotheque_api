import {ValidationChain} from "express-validator/src";
import {handleValidationErrors} from "../middlewares/handleValidationErrors";
import NextFunction = e.NextFunction;
import Response = e.Response;
import Request = e.Request;
import e = require("express");

/**
 * add a middleware that runs all validators, and throw a 402 if one of them fails
 * @param validators
 * @returns {(any|(req:Request, res:Response, next:NextFunction)=>(Response|NextFunction))[]}
 */
export function addValidationErrorHandler(validators: ValidationChain[]) {
    return [...validators, handleValidationErrors];
}
