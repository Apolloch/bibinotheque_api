import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
    JoinTable, OneToMany
} from "typeorm";
import {BeerType} from "./beerType";
import {Brand} from "./brand";
import moment = require("moment");
import Moment = moment.Moment;
import {BeerEvaluation} from "./beerEvaluation";

@Entity("beers")
export class Beer {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @JoinColumn({name: "beer_type_id"})
    @JoinTable()
    @ManyToOne((type) => BeerType, (beerType) => beerType.beers, {
        eager: true
    })
    public type?: BeerType;

    @JoinColumn({name: "brand_id"})
    @JoinTable()
    @ManyToOne((type) => Brand, (brand) => brand.beers, {
        eager: true
    })
    public brand?: Brand;

    @OneToMany((type) => BeerEvaluation, (beerEvaluation) => beerEvaluation.beer)
    public evaluations?: BeerEvaluation[];

    @CreateDateColumn({
        transformer: {
            to: (value: Moment): string => value.format("YYYY-MM-DD HH:mm:ss"),
            from: (value: string): Moment => moment(value, "YYYY-MM-DD HH:mm:ss")
        },
        name: "created_at"
    })
    public createdAt: Moment;

    @UpdateDateColumn({
        transformer: {
            to: (value: Moment): string => value.format("YYYY-MM-DD HH:mm:ss"),
            from: (value: string): Moment => moment(value, "YYYY-MM-DD HH:mm:ss")
        },
        name: "updated_at"
    })
    public updatedAt: Moment;
}
