import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn, OneToMany} from "typeorm";
import {Beer} from "./beer";
import moment = require("moment");
import Moment = moment.Moment;

@Entity("beer_types")
export class BeerType {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @OneToMany((type) => Beer, (beer) => beer.type)
    public beers?: Beer[];

    @CreateDateColumn({
        transformer: {
            to: (value: Moment): string => value.format("YYYY-MM-DD HH:mm:ss"),
            from: (value: string): Moment => moment(value, "YYYY-MM-DD HH:mm:ss")
        },
        name: "created_at"
    })
    public createdAt: Moment;

    @UpdateDateColumn({
        transformer: {
            to: (value: Moment): string => value.format("YYYY-MM-DD HH:mm:ss"),
            from: (value: string): Moment => moment(value, "YYYY-MM-DD HH:mm:ss")
        },
        name: "updated_at"
    })
    public updatedAt: Moment;

}
