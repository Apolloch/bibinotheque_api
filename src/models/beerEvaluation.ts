import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import moment = require("moment");
import Moment = moment.Moment;
import {Beer} from "./beer";
import {User} from "./user";

@Entity("beer_evaluations")
export class BeerEvaluation {

    @PrimaryGeneratedColumn()
    public id: number;

    @JoinColumn({name: "user_id"})
    @ManyToOne((type) => User, (user) => user.evaluations, {
        eager: true
    })
    public user: User;

    @JoinColumn({name: "beer_id"})
    @ManyToOne((type) => Beer, (beer) => beer.evaluations, {
        eager: true
    })
    public beer: Beer;

    @Column()
    public note: number;

    @Column()
    public comment: string;

    @CreateDateColumn({
        transformer: {
            to: (value: Moment): string => value.format("YYYY-MM-DD HH:mm:ss"),
            from: (value: string): Moment => moment(value, "YYYY-MM-DD HH:mm:ss")
        },
        name: "created_at"
    })
    public createdAt: Moment;

    @UpdateDateColumn({
        transformer: {
            to: (value: Moment): string => value.format("YYYY-MM-DD HH:mm:ss"),
            from: (value: string): Moment => moment(value, "YYYY-MM-DD HH:mm:ss")
        },
        name: "updated_at"
    })
    public updatedAt: Moment;
}
