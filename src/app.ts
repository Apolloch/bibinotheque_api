import express from "express";
import helmet from "helmet";
import routes from "./routes";
import dotenv from "dotenv";
import {handleStackErrors} from "./middlewares/handleStackErrors";
import NextFunction = express.NextFunction;
import Request = express.Request;
import Response = express.Response;

const app = express();

// load dotenv file
dotenv.config();
// middlewares
app.use(helmet());
app.use(express.json());
// routes
app.use("/", routes);
// error handlers : must be registered in last
app.use(handleStackErrors);

export default app;
