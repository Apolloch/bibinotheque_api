import {MigrationInterface, QueryRunner} from "typeorm";

export class BeerEvaluation1568975693565 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE beer_evaluations (" +
            "id SERIAL," +
            "beer_id INT," +
            "user_id INT," +
            "note NUMERIC," +
            "comment TEXT," +
            "created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "PRIMARY KEY (id)," +
            "CONSTRAINT unq_user_beer UNIQUE(beer_id,user_id)," +
            "FOREIGN KEY (beer_id) REFERENCES beers(id)," +
            "FOREIGN KEY (user_id) REFERENCES users(id))");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE beer_evaluations");
    }

}
