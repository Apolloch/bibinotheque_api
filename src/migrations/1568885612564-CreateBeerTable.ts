import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateBeerTable1568885612564 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE beers (" +
            "id SERIAL," +
            "name VARCHAR(255) NOT NULL ," +
            "beer_type_id INT," +
            "brand_id INT," +
            "created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "PRIMARY KEY (id)," +
            "FOREIGN KEY (beer_type_id) REFERENCES beer_types(id)," +
            "FOREIGN KEY (brand_id) REFERENCES brands(id))");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE beers");
    }

}
