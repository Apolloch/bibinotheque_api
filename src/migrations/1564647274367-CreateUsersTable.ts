import {MigrationInterface, QueryRunner} from "typeorm";

//noinspection JSUnusedGlobalSymbols
export class CreateUsersTable1564647274367 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("" +
            "CREATE TYPE user_role AS ENUM ('ADMIN','BASIC');" +
            "CREATE TABLE users (" +
            "id SERIAL," +
            "username VARCHAR(255) NOT NULL UNIQUE," +
            "email VARCHAR(255) NOT NULL UNIQUE," +
            "password VARCHAR(255) NOT NULL," +
            "role user_role DEFAULT 'BASIC'," +
            "created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "PRIMARY KEY (id) )");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE users");
    }

}
