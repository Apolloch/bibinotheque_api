import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateBeerTypeTable1568885598529 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE beer_types (" +
            "id SERIAL," +
            "name VARCHAR(255) NOT NULL ," +
            "created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "PRIMARY KEY (id));");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE beer_types");
    }
}
