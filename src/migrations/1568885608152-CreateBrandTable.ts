import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateBrandTable1568885608152 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE brands (" +
            "id SERIAL," +
            "name VARCHAR(255) NOT NULL ," +
            "created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "PRIMARY KEY (id));");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP TABLE brands");
    }

}
