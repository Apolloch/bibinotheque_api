import {UserRole} from "../src/models/user";
import moment from "moment";

export let userCollection = [ {
    id: 8,
    username: "test",
    password: "$2a$10$NOSXLEvAclWMmI2zE8v.7Oxa1zjzLe8fo6TqI32BYkscb6o8E2bzG",
    email: "aa@aa.aa",
    role: "BASIC",
    createdAt: "2019-09-11T11:43:37.000Z",
    updatedAt: "2019-09-11T11:43:37.000Z"
}];

export const dateToUse = moment();

export const insertId = 1;
export const userPayload = {
    email: "aa@aa.aa",
    username: "test",
    role : UserRole.BASIC
};
export const userWithoutPassword = {id: insertId, ...userPayload, createdAt: dateToUse, updatedAt: dateToUse};
export const userWithPassword = { ...userWithoutPassword, password: "123"};
