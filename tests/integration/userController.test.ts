import Mock = jest.Mock;
import express from "express";
import Request = express.Request;
import Response = express.Response;
import userService from "../../src/services/userService";
import app from "../../src/app";
import supertest from "supertest";
import {expectJson, expectOK, expectUnprocessableEntity} from "../testUtil";
import ErrorCode from "../../src/consts/ErrorCode";

let request = supertest(app);

jest.mock("../../src/services/userService");

describe("testing the userController ", () => {

    test("when a get request is made to the /users endpoint, " +
        "then it returns all the users", async() => {
        let testUsers = ["1", "2", "3"];
        // Arrange
        (userService.all as Mock).mockReturnValue(Promise.resolve(testUsers));
        // Act
        let result = await request.get("/users");
        // Assert
        expectOK(result);
        expectJson(result);
        expect(result.body).toStrictEqual(testUsers);
    });

    test("Given valid credentials, when a post request is made to the /users/auth endpoint, " +
        "then it returns the corresponding user and a token", async() => {
        // Arrange
        let authenticateReturn = {token: "123", user: "a user"};
        (userService.authenticate as Mock).mockReturnValue(Promise.resolve(authenticateReturn));
        // Act
        let result = await request.post("/users/auth").send({email: "aa@aa.aa", password: "aaa"});
        // Assert
        expectOK(result);
        expectJson(result);
        expect(result.body).toStrictEqual(authenticateReturn);
    });

    test("Given valid credentials, when a post request is made to the /users/auth endpoint, " +
        "then it returns a 401 with the good error code", async() => {
        // Arrange
        (userService.authenticate as Mock).mockReturnValue(Promise.resolve(false));
        // Act
        let result = await request.post("/users/auth").send({email: "aa@aa.aa", password: "aaa"});
        // Assert
        expect(result.statusCode).toBe(401);
        expectJson(result);
        expect(result.body.code).toBe(ErrorCode.UNAUTHORIZED);
    });

    test("Given invalid credentials, when a post request is made to the /users/auth endpoint, " +
        "then it returns a 422, with the good messages", async() => {
        // Arrange
        (userService.authenticate as Mock).mockImplementation(async() => {
            throw Error();
        });
        // Act
        let result = await request.post("/users/auth").send({email: "aaaa"});
        // Assert
        expect(result.statusCode).toBe(422);
        expectJson(result);
        expect(result.body.errors.length).toBe(2);
    });

    test("Given valid datas, when a post request is made to the /users endpoint, " +
        "then it returns the created user", async() => {
        // Arrange
        let userCreated = {name: "tutu"};
        (userService.register as Mock).mockReturnValue(Promise.resolve(userCreated));
        // Act
        let result = await request.post("/users").send({
            email: "aa@aa.aa",
            password: "123",
            username: "blabla"
        });
        // Assert
        expect(result.statusCode).toBe(200);
        expectJson(result);
        expect(result.body).toStrictEqual(userCreated);
    });

    test("Given invalid datas, when a post request is made to the /users endpoint, " +
        "then it returns a 422, with the good messages", async() => {
        // Arrange
        let userCreated = {name: "tutu"};
        (userService.register as Mock).mockReturnValue(Promise.resolve(userCreated));
        // Act
        let result = await request.post("/users").send({
            email: "aa.aa",
            password: "",
        });
        // Assert
        expectUnprocessableEntity(result);
        expectJson(result);
        expect(result.body.errors.length).toBe(3);
    });

    test("Given a userId, when a get request is made to the /users/:userId endpoint, " +
        "then it returns the user", async() => {
        // Arrange
        let user = {name: "tutu"};
        (userService.findOne as Mock).mockReturnValue(Promise.resolve(user));
        // Act
        let result = await request.get("/users/1");
        // Assert
        expectOK(result);
        expectJson(result);
        expect(result.body).toEqual(user);
    });

    test("Given a userId, when a get request is made to the /users/:userId/evaluations endpoint, " +
        "then it returns the user's evaluations", async() => {
        // Arrange
        let evaluations = ["1", "2"];
        (userService.getEvaluations as Mock).mockReturnValue(Promise.resolve(evaluations));
        // Act
        let result = await request.get("/users/1/evaluations");
        // Assert
        expectOK(result);
        expectJson(result);
        expect(result.body).toEqual(evaluations);
    });
});
