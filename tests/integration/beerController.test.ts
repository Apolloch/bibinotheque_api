import Mock = jest.Mock;
import express from "express";
import Request = express.Request;
import Response = express.Response;
import beerService from "../../src/services/beerService";
import app from "../../src/app";
import supertest from "supertest";
import {expectJson, expectOK, expectUnprocessableEntity} from "../testUtil";

let request = supertest(app);

jest.mock("../../src/services/beerService");

describe("testing the beerController ", () => {

    test("when a get request is made to the /beers endpoint, " +
        "then it returns all the beers", async() => {
        let testBeers = ["1", "2", "3"];
        // Arrange
        (beerService.all as Mock).mockReturnValue(Promise.resolve(testBeers));
        // Act
        let result = await request.get("/beers");
        // Assert
        expectOK(result);
        expectJson(result);
        expect(result.body).toStrictEqual(testBeers);
    });

    test("Given valid datas, when a post request is made to the /beers endpoint, " +
        "then it returns the created beer", async() => {
        let testBeer = "a beer";
        // Arrange
        (beerService.createOne as Mock).mockReturnValue(Promise.resolve(testBeer));
        // Act
        let result = await request.post("/beers").send({
            name: "A beer",
            beerTypeId: 1,
            brandId: 1
        });
        // Assert
        expectOK(result);
        expectJson(result);
        expect(result.body).toStrictEqual(testBeer);
    });

    test("Given invalid datas, when a post request is made to the /beers endpoint, " +
        "then it returns the created beer", async() => {
        let testBeer = "a beer";
        // Arrange
        (beerService.createOne as Mock).mockReturnValue(Promise.resolve(testBeer));
        // Act
        let result = await request.post("/beers").send({
            name: "",
            beerTypeId: "Not a number"
        });
        // Assert
        expectUnprocessableEntity(result);
        expectJson(result);
        expect(result.body.errors.length).toBe(3);
    });

});
