export function expectJson(response) {
    return expect(response.headers["content-type"]).toBe("application/json; charset=utf-8");
}

export function expectOK(response) {
    return expectStatus(response, 200);
}
export function expectUnprocessableEntity(response) {
    return expectStatus(response, 422);
}

function expectStatus(response, status) {
    return expect(response.status).toBe(status);
}
