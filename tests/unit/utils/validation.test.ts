import Mock = jest.Mock;
import {handleValidationErrors} from "../../../src/middlewares/handleValidationErrors";
import {addValidationErrorHandler} from "../../../src/utils/validation";
import {ValidationChain} from "express-validator/src";
jest.mock("../../../src/middlewares/handleValidationErrors");

beforeEach(() => {
    jest.resetModules();
});
describe("testing the validation util", () => {

    test("Given a validator array, when addValidationErrorHandler is called, " +
        "then it returns the array with all validator plus a middleware that check the validator results", async() => {
        // Arrange
        let validators = ["1", "2"];
        // Act
        let addValErrHandReturn = addValidationErrorHandler(validators as any as ValidationChain[]);
        // Assert
        expect(addValErrHandReturn).toEqual([...validators, handleValidationErrors]);
    });

});
