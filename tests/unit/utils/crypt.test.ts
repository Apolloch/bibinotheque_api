import {checkPassword, hashPassword} from "../../../src/utils/crypt";
import bcrypt from "bcryptjs";
import Mock = jest.Mock;

jest.mock("bcryptjs");

let hashedPwd = "hash";
let password = "pwd";

describe("testing the crypt util", () => {

    test("Given a password, when hashPassword is called, then it returns its hash", async() => {
        // Arrange
        (bcrypt.hash as Mock).mockReturnValue(hashedPwd);
        // Act
        let hashReturn = await hashPassword(password);
        // Assert
        expect(hashReturn).toEqual(hashedPwd);
    });

    test("Given a password and its corresponding hash, when checkPassword is called, then it returns true", async() => {
        // Arrange
        (bcrypt.compare as Mock).mockReturnValue(true);
        // Act
        let hashReturn = await checkPassword(password, hashedPwd);
        // Assert
        expect(hashReturn).toBe(true);
    });

    test("Given a wrong password and a hash, when checkPassword is called, then it returns false", async() => {
        // Arrange
        (bcrypt.compare as Mock).mockReturnValue(false);
        // Act
        let hashReturn = await checkPassword(password, hashedPwd);
        // Assert
        expect(hashReturn).toBe(false);
    });

});
