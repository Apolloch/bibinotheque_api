import {createJwt, checkJwtValidity} from "../../../src/utils/jwt";
import jwt from "jsonwebtoken";
import moment from "moment";
import Mock = jest.Mock;
import {UserRole} from "../../../src/models/user";

jest.mock("jsonwebtoken");
jest.mock("moment");

let testUser = {
    id: 8,
    username: "test",
    password: "$2a$10$NOSXLEvAclWMmI2zE8v.7Oxa1zjzLe8fo6TqI32BYkscb6o8E2bzG",
    email: "aa@aa.aa",
    role: UserRole.BASIC,
    createdAt: moment(),
    updatedAt: moment()
};
let jwtReturn = "truc";

const OLD_ENV = process.env;

beforeEach(() => {
    jest.resetModules(); // this is important - it clears the cache
    process.env = {...OLD_ENV};
});

afterEach(() => {
    process.env = OLD_ENV;
});

describe("testing the jwt util", () => {

    test("Given a user, when createJwt is called," +
        " then it returns a jwt with the userId, the userRole, and the correct exp", async() => {
        // Arrange;
        (jwt.sign as Mock).mockReturnValue(jwtReturn);

        let unixReturn = "unixReturn";
        let addFn: Mock = jest.fn(() => ({unix: jest.fn(() => unixReturn)}));
        (moment as any as Mock).mockReturnValue({add: addFn});

        let accessTokenDuration = "ANYTHING";
        process.env.ACCESS_TOKEN_DURATION = accessTokenDuration;

        let jwtSignExpectedPayload = {
            data: {
                userId: testUser.id,
                userRole: testUser.role,
            },
            exp: unixReturn
        };
        // Act
        let jwtResult = createJwt(testUser);
        // Assert
        expect(addFn.mock.calls[0][0]).toBe(accessTokenDuration);
        expect((jwt.sign as Mock).mock.calls[0][0]).toStrictEqual(jwtSignExpectedPayload);
        expect(jwtResult).toBe(jwtResult);
    });

    test("Given a good token, when checkJwtValidity is called," +
        " then it returns the jwt data payload", async() => {
        // Arrange;
        let verifyReturn = "verifyreturn";
        (jwt.verify as Mock).mockReturnValue(verifyReturn);
        // Act
        let checkJwtValidityResult = checkJwtValidity("");
        // Assert
        expect(checkJwtValidityResult).toBe(verifyReturn);
    });

    test("Given a malformed token, when checkJwtValidity is called," +
        " then it throws the corresponding error", async() => {
        // Arrange;
        (jwt.verify as Mock).mockImplementation(() => {
            throw new Error("JsonWebTokenError");
        });
        // Act and Assert
        expect(() => checkJwtValidity("")).toThrow("JsonWebTokenError");
    });

    test("Given an expired token, when checkJwtValidity is called," +
        " then it throws the corresponding error", async() => {
        // Arrange;
        (jwt.verify as Mock).mockImplementation(() => {
            throw new Error("TokenExpiredError");
        });
        // Act and Assert
        expect(() => checkJwtValidity("")).toThrow("TokenExpiredError");
    });

});
