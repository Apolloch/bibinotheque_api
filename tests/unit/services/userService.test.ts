import userService from "../../../src/services/userService";
import {userCollection, userWithPassword, userWithoutPassword} from "../../testDataSet";
import {checkPassword} from "../../../src/utils/crypt";
import {createJwt} from "../../../src/utils/jwt";
import userRepository from "../../../src/repositories/userRepository";
import {EmailAlreadyExistsException} from "../../../src/exceptions/EmailAlreadyExistsException";
import {UsernameAlreadyExistsException} from "../../../src/exceptions/UsernameAlreadyExistsException";
import Mock = jest.Mock;

jest.mock("../../../src/repositories/userRepository");
jest.mock("../../../src/utils/jwt");
jest.mock("../../../src/utils/crypt");
jest.mock("../../../src/models/user");

let email = "aa@aa.aa";
let password = "test";
let jwtValue = "the jwt";

describe("testing the user service", () => {

    test("When all is called, then it returns all users", async() => {
        // Arrange
        (userRepository.all as Mock).mockReturnValue(Promise.resolve(userCollection));
        // Act
        let allResult = await userService.all();
        // Assert
        expect(allResult).toEqual(userCollection);
    });

    test("Given a userId, when findOne is called, then it returns the user", async() => {
        // Arrange
        let testUser = "aaaa";
        (userRepository.findOneById as Mock).mockReturnValue(Promise.resolve(testUser));
        // Act
        let result = await userService.findOne(1);
        // Assert
        expect(result).toEqual(testUser);
    });

    test("Given a userId, when getEvaluations is called, then it returns the user's evaluations", async() => {
        // Arrange
        let evaluations = ["1", "2"];
        (userRepository.getEvaluations as Mock).mockReturnValue(Promise.resolve(evaluations));
        // Act
        let result = await userService.getEvaluations(1);
        // Assert
        expect(result).toEqual(evaluations);
    });

    test("Given an unknown email, when authenticate is called, then it returns false", async() => {
        // Arrange
        (userRepository.findOneByEmail as Mock).mockReturnValue(null);
        // Act
        let authResult = await userService.authenticate(email, password);
        // Assert
        expect(authResult).toBe(false);
    });

    test("Given a wrong password, when authenticate is called, then it returns false", async() => {
        // Arrange
        (userRepository.findOneByEmail as Mock).mockReturnValue(userWithPassword);
        (checkPassword as Mock).mockReturnValue(Promise.resolve(false));
        // Act
        let authResult = await userService.authenticate(email, password);
        // Assert
        expect(authResult).toBe(false);
    });

    test("Given good credentials, when authenticate is called, then it returns the token and the user", async() => {
        // Arrange
        (userRepository.findOneByEmail as Mock).mockReturnValue(userWithPassword);
        (checkPassword as Mock).mockReturnValue(Promise.resolve(true));
        (createJwt as Mock).mockReturnValue(jwtValue);
        // Act
        let authResult = await userService.authenticate(email, password);
        // Assert
        expect(authResult).toStrictEqual({token: jwtValue, user: userWithoutPassword});
    });

    test("Given an email that already exists, when register is called," +
        "then it throws the corresponding exception", async() => {
        // Arrange
        (userRepository.findOneByEmail as Mock).mockReturnValue(userWithoutPassword);
        // Act
        let register = userService.register(userWithoutPassword.username, password, userWithoutPassword.email);
        // Assert
        expect(register).rejects.toBeInstanceOf(EmailAlreadyExistsException);
    });

    test("Given a login that exists and an email that doesn't, when register is called, " +
        "then it throws the corresponding exception", async() => {
        // Arrange
        (userRepository.findOneByEmail as Mock).mockReturnValueOnce(null);
        (userRepository.findOneByUsername as Mock).mockReturnValue(userWithoutPassword);
        // Act
        let register = userService.register(userWithoutPassword.username, password, userWithoutPassword.email);
        // Assert
        expect(register).rejects.toBeInstanceOf(UsernameAlreadyExistsException);
    });

    test("Given an email and a username that doesn't exists, when register is called," +
        " then it returns the created user", async() => {
        (userRepository.findOneByEmail as Mock).mockReturnValue(null);
        (userRepository.findOneByUsername as Mock).mockReturnValue(null);
        (userRepository.createOne as Mock).mockReturnValue(userWithoutPassword);
        let returnedUser = await userService
            .register(userWithoutPassword.username, password, userWithoutPassword.email);
        expect(returnedUser).toStrictEqual(userWithoutPassword);
    });
});
