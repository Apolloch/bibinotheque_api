import beerRepository from "../../../src/repositories/beerRepository";
import beerService from "../../../src/services/beerService";
import Mock = jest.Mock;

jest.mock("../../../src/repositories/beerRepository");

describe("testing the beer service", () => {

    test("When all is called, then it returns all beers", async() => {
        // Arrange
        let beerCollection = ["1", "2", "3"];
        (beerRepository.all as Mock).mockReturnValue(Promise.resolve(beerCollection));
        // Act
        let allResult = await beerService.all();
        // Assert
        expect(allResult).toEqual(beerCollection);
    });

    test("When createOne is called, then it returns it return the created beer", async() => {
        // Arrange
        let testBeer = "a beer";
        let beerName = "A beer";
        let beerTypeId = 1;
        let brandId = 1;
        (beerRepository.createOne as Mock).mockReturnValue(Promise.resolve(testBeer));
        // Act
        let result = await beerService.createOne(beerName, beerTypeId, brandId);
        // Assert
        expect(result).toBe(testBeer);
    });

});
