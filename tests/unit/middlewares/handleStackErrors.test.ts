import Mock = jest.Mock;
import express from "express";
import Request = express.Request;
import Response = express.Response;
import rewire from "rewire";
import {handleStackErrors} from "../../../src/middlewares/handleStackErrors";

const OLD_ENV = process.env;

beforeEach(() => {
    process.env = {...OLD_ENV};
});

describe("testing the handleStackErrors middleware", () => {

    test("Given a Base exception error, when handleStackErrors is called" +
        "then it calls its supplied res object with the exception status, code and message as json", async() => {
        // Arrange
        let baseException = {
            message: "test",
            code: "CODE",
            status: 900
        };
        const handleStackErrorsModule = rewire("./../../../dist/middlewares/handleStackErrors.js");
        handleStackErrorsModule.__set__("isBaseException", () => true);
        let nextMock = jest.fn();
        let jsonMock = jest.fn();
        let resMock = {status: jest.fn(() => ({json: jsonMock}))};
        // Act
        handleStackErrorsModule
            .handleStackErrors(baseException, jest.fn() as any as Request, resMock as any as Response, nextMock);
        // Assert
        expect(resMock.status.mock.calls[0]).toEqual([baseException.status]);
        expect(jsonMock.mock.calls[0]).toEqual([{message: baseException.message, code: baseException.code}]);
    });

    test("Given a generic error and a non prod env, when handleStackErrors is called" +
        "then it calls its supplied res object with the 500 status, and its message as json", async() => {
        // Arrange
        process.env.ENV = "dev";
        let errorMessage = "test";
        let err = Error(errorMessage);
        let nextMock = jest.fn();
        let jsonMock = jest.fn();
        let resMock = {status: jest.fn(() => ({json: jsonMock}))};
        // Act
        handleStackErrors(err, jest.fn() as any as Request, resMock as any as Response, nextMock);
        // Assert
        expect(resMock.status.mock.calls[0]).toEqual([500]);
        expect(jsonMock.mock.calls[0]).toEqual([{message: errorMessage}]);
    });

    test("Given a generic error and a prod env, when handleStackErrors is called" +
        "then it calls its supplied res object with the 500 status, and its message as json", async() => {
        // Arrange
        process.env.ENV = "prod";
        let errorMessage = "test";
        let err = Error(errorMessage);
        let nextMock = jest.fn();
        let jsonMock = jest.fn();
        let resMock = {status: jest.fn(() => ({json: jsonMock}))};
        // Act
        handleStackErrors(err, jest.fn() as any as Request, resMock as any as Response, nextMock);
        // Assert
        expect(resMock.status.mock.calls[0]).toEqual([500]);
        expect(jsonMock.mock.calls[0]).toEqual([{message: "Something broke!"}]);
    });

});
