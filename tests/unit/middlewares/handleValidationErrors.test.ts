import Mock = jest.Mock;
import {validationResult} from "express-validator/src";
import {handleValidationErrors} from "../../../src/middlewares/handleValidationErrors";
import express from "express";
import Request = express.Request;
import Response = express.Response;

jest.mock("express-validator/src");

beforeEach(() => {
    jest.resetModules();
});
describe("testing the handleValidationErrors middleware", () => {

    test("Given a nonempty validation result, when handleValidationErrors is called, " +
        "then it calls its supplied res object with 422 status and the errors as json", async() => {
        // Arrange
        let valResult = ["this is a result"];
        (validationResult as any as Mock).mockReturnValue(
            {
                isEmpty: jest.fn(() => false),
                array: jest.fn(() => valResult)
            }
        );
        let nextMock = jest.fn();
        let jsonMock = jest.fn();
        let resMock = {status: jest.fn(() => ({json: jsonMock}))};
        // Act
        handleValidationErrors(jest.fn() as any as Request, resMock as any as Response, nextMock);
        // Assert
        expect(resMock.status.mock.calls[0]).toEqual([422]);
        expect(jsonMock.mock.calls[0]).toEqual([{errors: valResult}]);
    });

    test("Given an empty validation result, when handleValidationErrors is called, " +
        "then it calls its supplied next function without args", async() => {
        // Arrange
        (validationResult as any as Mock).mockReturnValue({isEmpty: jest.fn(() => true)});
        let nextMock = jest.fn();
        // Act
        handleValidationErrors(jest.fn() as any as Request, jest.fn() as any as Response, nextMock);
        // Assert
        expect(nextMock.mock.calls.length).toBe(1);
        expect(nextMock.mock.calls[0]).toEqual([]);
    });

});
