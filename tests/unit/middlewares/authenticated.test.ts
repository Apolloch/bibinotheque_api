import Mock = jest.Mock;
import express from "express";
import Request = express.Request;
import Response = express.Response;
import authenticatedMiddleware from "../../../src/middlewares/authenticated";
import UnauthorizedException from "../../../src/exceptions/UnauthorizedException";
import {checkJwtValidity} from "../../../src/utils/jwt";
import JwtExpiredException from "../../../src/exceptions/JwtExpiredException";

jest.mock("../../../src/utils/jwt");

beforeEach(() => {
    jest.resetModules();
});
describe("testing the authenticatedMiddleware middleware", () => {

    test("Given a request without the Authorization header, when authenticatedMiddleware is called, " +
        "then it calls its supplied next function with the correct exception", async() => {
        // Arrange
        let reqMock = {header: jest.fn()};
        // duplicata necessary otherwise the test fails
        let nextMock = jest.fn();
        let resMock = jest.fn();

        // Act
        authenticatedMiddleware(reqMock as any as Request, resMock as any as Response, nextMock);
        // Assert
        expect(nextMock.mock.calls[0][0]).toBeInstanceOf(UnauthorizedException);
    });

    test("Given an expired token, when authenticatedMiddleware is called, " +
        "then it calls its supplied next function with the correct exception", async() => {
        // Arrange
        let reqMock = {header: jest.fn(() => "fakeJwtHeader")};
        // duplicata necessary otherwise the test fails
        let nextMock = jest.fn();
        let resMock = jest.fn();

        (checkJwtValidity as Mock).mockImplementation(() => {
            throw {name: "TokenExpiredError"};
        });
        // Act
        authenticatedMiddleware(reqMock as any as Request, resMock as any as Response, nextMock);
        // Assert
        expect(nextMock.mock.calls[0][0]).toBeInstanceOf(JwtExpiredException);
    });

    test("Given an invalid token, when authenticatedMiddleware is called, " +
        "then it calls its supplied next function with the correct exception", async() => {
        // Arrange
        let reqMock = {header: jest.fn(() => "fakeJwtHeader")};
        let nextMock = jest.fn();
        let resMock = jest.fn();
        (checkJwtValidity as Mock).mockImplementation(() => {
            throw {name: "JsonWebTokenError"};
        });
        // Act
        authenticatedMiddleware(reqMock as any as Request, resMock as any as Response, nextMock);
        // Assert
        expect(nextMock.mock.calls[0][0]).toBeInstanceOf(UnauthorizedException);
    });

    test("Given an unknown error throw in the jwt lib, when authenticatedMiddleware is called, " +
        "then it calls its supplied next function with the thrown error", async() => {
        // Arrange
        let reqMock = {header: jest.fn(() => "fakeJwtHeader")};
        let nextMock = jest.fn();
        let resMock = jest.fn();
        let anyError = {name: "unknown"};
        (checkJwtValidity as Mock).mockImplementation(() => {
            throw anyError;
        });
        // Act
        authenticatedMiddleware(reqMock as any as Request, resMock as any as Response, nextMock);
        // Assert
        expect(nextMock.mock.calls[0][0]).toStrictEqual(anyError);
        expect(nextMock.mock.calls[0][0]).not.toBeInstanceOf(UnauthorizedException);
        expect(nextMock.mock.calls[0][0]).not.toBeInstanceOf(JwtExpiredException);
    });

    test("Given a valid token, when authenticatedMiddleware is called, " +
        "then it calls its supplied next function with no parameter," +
        "and fills the req parameter with the jwt payload", async() => {
        // Arrange
        let reqMock = {
            header: jest.fn(() => "fakeJwtHeader"),
            userId: 0,
            userRole: "nothing"
        };
        let nextMock = jest.fn();
        let resMock = jest.fn();
        let anyError = {name: "unknown"};
        let testId = 1;
        let testRole = "a role";
        (checkJwtValidity as Mock).mockImplementation(() => {
            return {
                data: {
                    userId: testId,
                    userRole: testRole
                }
            };
        });
        // Act
        authenticatedMiddleware(reqMock as any as Request, resMock as any as Response, nextMock);
        // Assert
        expect(nextMock.mock.calls[0].length).toBe(0);
        expect(reqMock.userId).toBe(testId);
        expect(reqMock.userRole).toBe(testRole);
    });

});
