# Bibinotheque Api

[![pipeline status](https://gitlab.com/Apolloch/bibinotheque_api/badges/master/pipeline.svg)](https://gitlab.com/Apolloch/bibinotheque_api/commits/master)
[![coverage report](https://gitlab.com/Apolloch/bibinotheque_api/badges/master/coverage.svg)](https://gitlab.com/Apolloch/bibinotheque_api/commits/master)

The stagging environnement is available here : https://bibinotheque-api-staging.herokuapp.com/

## Building the project

### Install dependencies
Run the following command to install the dependencies:

```npm install```

### Build the project

#### Config

- Copy the .env.example as .env
- Fill the TYPEORM_* fields according to your database setup
- Fill the JWT_SECRET field with a long enough random string. This string
will be used to encrypt the jwt tokens

#### Transpile

run the following command to build the /dist directory:

```npm run build```


### Run the project

Run the following command in order to start the server:

```npm run start```

Be sure that the project is built before starting it

### Testing the project

run the following command to run all the automated tests:

```npm test```

if you want to run a specific test suite, run the following command:

```npm test -- nameOfTheTestFileYouWantToRun```

Please not the space between the two dashes and test file name

### Dev command

While working on this project, i highly recommand using the following command:

```npm run watch```

It watch all the files, and rebuild and restart the server when a change is detected